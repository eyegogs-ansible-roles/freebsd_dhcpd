# freebsd_dhcpd
A role to configure a DHCP server on a FreeBSD host

Currently only IPv4 is supported.

## Variables
- `dhcpd_domain_name` (**Mandatory**): Domain that is provided to clients as the default search domain
- `dhcpd_global_name_servers` [List]: List of nameservers to provide to clients, globally in dhcpd.conf
- `dhcpd_default_lease_time`: Default DHCP lease time, defaults to `3600`
- `dhcpd_max_lease_time`: Max DHCP lease time, defaults to `86400`
- `dhcpd_subnets` [List]: (**Mandatory**) A list of subnets that this server has DHCP pools for. Each subnet consists of the following fields:
  - `addr` (**Mandatory**): The network address of this subnet
  - `mask` (**Mandatory**): The mask of this subnet
  - `gateway` (**Mandatory**): The gateway to provide to DHCP clients
  - `start_addr` (**Mandatory**): The starting address of the DHCP pool
  - `end_addr` (**Mandatory**): The ending address of the DHCP pool
  - `name_servers` [List]: List of name servers that will be provided to clients in this subnet
- `dhcpd_static_leases` [List]: List of static leases in this server, each lease consist of the following 2 fields:
  - `mac`: The mac address of the host
  - `addr`: The IPv4 address of the host
